<?php 

return [
	'menu' => [
		[
			'type' 		=> 'single',
	        'name' 		=> 'Trang chủ',
	        'icon' 		=> 'fas fa-tachometer-alt',
	        'route' 	=> 'admin.home',
	        'role'		=> 'home'
		],
	    [
	        'type' 		=> 'group',
	        'name' 		=> 'Quản trị nội dung',
	        'role' 		=> [
	        	'posts_create',
	        	'posts_index',
	        	'post_categories_index',
	        	'pages_create',
	        	'pages_index',
	        	'tags_index',
	        ]
	    ],
	    [
	    	'type' 				=> 'multiple',
	    	'name' 				=> 'Bài viết',
			'icon' 				=> 'fas fa-file-alt',
			'childs' => [
				[
					'name' 		=> 'Thêm mới',
					'route' 	=> 'admin.posts.create',
					'role' 		=> 'posts_create'
				],
				[
					'name' 		=> 'Danh sách',
					'route' 	=> 'admin.posts.index',
					'role' 		=> 'posts_index',
					'active' 	=> [ 'admin.posts.show', 'admin.posts.edit' ]
				],
				[
					'name' 		=> 'Danh mục',
					'route' 	=> 'admin.post_categories.index',
					'role' 		=> 'post_categories_index',
					'active' 	=> [ 'admin.post_categories.create', 'admin.post_categories.show', 'admin.post_categories.edit' ]
				]
			]
	    ],
	    [
	    	'type' 				=> 'multiple',
	    	'name' 				=> 'Trang đơn',
			'icon' 				=> 'fas fa-file',
			'childs' => [
				[
					'name' 		=> 'Thêm mới',
					'route' 	=> 'admin.pages.create',
					'role' 		=> 'pages_create'
				],
				[
					'name' 		=> 'Danh sách',
					'route' 	=> 'admin.pages.index',
					'role' 		=> 'pages_index',
					'active' 	=> [ 'admin.pages.show', 'admin.pages.edit' ]
				]
			]
	    ],
	    [
	    	'type' 		=> 'single',
			'name' 		=> 'Quản lý Tags',
			'icon' 		=> 'fas fa-tags',
			'route' 	=> 'admin.tags.index',
			'role'		=> 'tags_index'
		],
	    [
	        'type' 		=> 'group',
	        'name' 		=> 'Cấu hình',
	        'role' 		=> [
	        	'settings_general',
	        	'settings_home',
	        	'settings_seo',
	        	'settings_mail_config',
	        	'sync_links_index'
	        ]
	    ],
	    [
	    	'type' 		=> 'single',
			'name' 		=> 'Cấu hình chung',
			'icon' 		=> 'fas fa-cogs',
			'route' 	=> 'admin.settings.general',
			'role'		=> 'settings_general'
		],
		[
	    	'type' 		=> 'single',
			'name' 		=> 'Cấu hình SEO',
			'icon' 		=> 'fab fa-stripe-s',
			'route' 	=> 'admin.settings.seo',
			'role'		=> 'settings_seo'
		],
		[
			'type' 		=> 'single',
			'name' 		=> 'Cấu hình Mail',
			'icon' 		=> 'fas fa-envelope',
			'route' 	=> 'admin.settings.mail_config',
			'role'		=> 'settings_mail_config'
		],
		[
	    	'type' 				=> 'multiple',
	        'name' 				=> 'Cấu hình giao diện',
	        'icon' 				=> 'fab fa-artstation',
	        'childs' => [
	        	[
					'name' 		=> 'Cấu hình trang chủ',
					'route' 	=> 'admin.settings.home',
					'role'		=> 'settings_home'
				],
	            
	        ]
	    ],
		[
	    	'type' 		=> 'single',
			'name' 		=> 'Link đồng bộ',
			'icon' 		=> 'fas fa-link',
			'route' 	=> 'admin.sync_links.index',
			'role'		=> 'sync_links_index'
		],
		[
	        'type' 		=> 'group',
	        'name' 		=> 'Tài khoản',
	        'role' 		=> [
	        	'admin_users_create',
	        	'admin_users_index'
	        ],
	    ],
	    [
	    	'type' 				=> 'multiple',
	        'name' 				=> 'Tài khoản quản trị',
	        'icon' 				=> 'fas fa-users',
	        'childs' => [
	            [
	                'name' 		=> 'Thêm mới',
	                'route' 	=> 'admin.admin_users.create',
	                'role' 		=> 'admin_users_create'
	            ],
	            [
	                'name' 		=> 'Danh sách',
	                'route' 	=> 'admin.admin_users.index',
	                'role' 		=> 'admin_users_index',
	                'active' 	=> [ 'admin.admin_users.show', 'admin.admin_users.edit' ]
	            ]
	        ]
	    ],
	    [
	        'type' 		=> 'group',
	        'name' 		=> 'Khác',
	        'role' 		=> [
	        	'media_view',
	        	'logs_view',
	        	'adminer_view',
	        	'system_logs_index'
	        ]
	    ],
	    [
	    	'type' 		=> 'single',
			'name' 		=> 'Quản lý tập tin',
			'icon' 		=> 'fas fa-photo-video',
			'route' 	=> 'media.view',
			'role'		=> 'media_view'
		],
		[
	    	'type' 		=> 'single',
			'name' 		=> 'Quản lý Log',
			'icon' 		=> 'fas fa-bug',
			'route' 	=> 'admin.logs.view',
			'role'		=> 'logs_view'
		],
		[
			'type' 		=> 'single',
			'name' 		=> 'Lịch sử hệ thống',
			'icon' 		=> 'fas fa-redo',
			'route' 	=> 'admin.system_logs.index',
			'role'		=> 'system_logs_index',
			'active' 	=> [ 'admin.system_logs.show' ]
		]
	],
];