$(document).ready(function() {
	var storage = {
		'body-small-text': false,
		// 'fixed-sidebar': true,
		'fixed-header': true,
		'sidebar-nav-compact': true,
	};
	$.each(storage, function(index, item) {
		if (getLocalStorage(index) == null) {
			setLocalStorage(index, item);
		}
	})
	var navbar_skins = [
		'navbar-primary',
		'navbar-secondary',
		'navbar-info',
		'navbar-success',
		'navbar-danger',
		'navbar-indigo',
		'navbar-purple',
		'navbar-pink',
		'navbar-navy',
		'navbar-lightblue',
		'navbar-teal',
		'navbar-cyan',
		'navbar-dark',
		'navbar-gray-dark',
		'navbar-gray',
		'navbar-light',
		'navbar-warning',
		'navbar-white',
		'navbar-orange',
	];
	var sidebar_colors = [
		'bg-primary',
		'bg-warning',
		'bg-info',
		'bg-danger',
		'bg-success',
		'bg-indigo',
		'bg-lightblue',
		'bg-navy',
		'bg-purple',
		'bg-fuchsia',
		'bg-pink',
		'bg-maroon',
		'bg-orange',
		'bg-lime',
		'bg-teal',
		'bg-olive'
	];
	var accent_colors = [
		'accent-white',
		'accent-primary',
		'accent-warning',
		'accent-info',
		'accent-danger',
		'accent-success',
		'accent-indigo',
		'accent-lightblue',
		'accent-navy',
		'accent-purple',
		'accent-fuchsia',
		'accent-pink',
		'accent-maroon',
		'accent-orange',
		'accent-lime',
		'accent-teal',
		'accent-olive'
	]
	var sidebar_dark_skins = [
		'sidebar-dark-primary',
		'sidebar-dark-warning',
		'sidebar-dark-info',
		'sidebar-dark-danger',
		'sidebar-dark-success',
		'sidebar-dark-indigo',
		'sidebar-dark-lightblue',
		'sidebar-dark-navy',
		'sidebar-dark-purple',
		'sidebar-dark-fuchsia',
		'sidebar-dark-pink',
		'sidebar-dark-maroon',
		'sidebar-dark-orange',
		'sidebar-dark-lime',
		'sidebar-dark-teal',
		'sidebar-dark-olive',
	];
	var sidebar_light_skins = [
		'sidebar-light-primary',
		'sidebar-light-warning',
		'sidebar-light-info',
		'sidebar-light-danger',
		'sidebar-light-success',
		'sidebar-light-indigo',
		'sidebar-light-lightblue',
		'sidebar-light-navy',
		'sidebar-light-purple',
		'sidebar-light-fuchsia',
		'sidebar-light-pink',
		'sidebar-light-maroon',
		'sidebar-light-orange',
		'sidebar-light-lime',
		'sidebar-light-teal',
		'sidebar-light-olive'
	];
	// Thẻ chứa
	var $sidebar   = $('.control-sidebar');
	$sidebar.append(`
		<div class="p-3 control-sidebar-content">
			<h5>Customize AdminLTE</h5>
			<hr class="mb-2"/>
		</div>
	`);
	// Tạo thẻ chứa các item
	var $container = $('.control-sidebar-content');

	// Label
	$container.append('<h6 class="mt-3">General</h6>');

	// body-small-text
	body_small_text_id = 'body-small-text';
	body_small_text_text = 'Body Small Text';
	body_small_text_name = 'text-sm';
	body_small_text_action = 'body';
	createCheckBlock(body_small_text_text, body_small_text_id);
	$('body').on('click', '#'+body_small_text_id, function() {
		
		if ($(this).is(':checked')) {
			setLocalStorage(body_small_text_id, true);
			$(body_small_text_action).addClass(body_small_text_name);
		} else {
			setLocalStorage(body_small_text_id, false);
			$(body_small_text_action).removeClass(body_small_text_name);
		}
	});
	if (getLocalStorage(body_small_text_id) == 'true') {
		$('#'+body_small_text_id).click();
	} else {
		$(body_small_text_action).removeClass(body_small_text_name);
	}

	// fixed-sidebar
	// fixed_sidebar_id = 'fixed-sidebar';
	// fixed_sidebar_text = 'Fixed Sidebar';
	// fixed_sidebar_name = 'layout-fixed';
	// fixed_sidebar_action = 'body';
	// createCheckBlock(fixed_sidebar_text, fixed_sidebar_id);
	// $('body').on('click', '#'+fixed_sidebar_id, function() {
		
	// 	if ($(this).is(':checked')) {
	// 		setLocalStorage(fixed_sidebar_id, true);
	// 		$(fixed_sidebar_action).addClass(fixed_sidebar_name);
	// 	} else {
	// 		setLocalStorage(fixed_sidebar_id, false);
	// 		$(fixed_sidebar_action).removeClass(fixed_sidebar_name);
	// 	}
	// });
	// if (getLocalStorage(fixed_sidebar_id) == 'true') {
	// 	$('#'+fixed_sidebar_id).click();
	// } else {
	// 	$(fixed_sidebar_action).removeClass(fixed_sidebar_name);
	// }

	// fixed-header
	fixed_header_id = 'fixed-header';
	fixed_header_text = 'Fixed Header';
	fixed_header_name = 'layout-navbar-fixed';
	fixed_header_action = 'body';
	createCheckBlock(fixed_header_text, fixed_header_id);
	$('body').on('click', '#'+fixed_header_id, function() {
		
		if ($(this).is(':checked')) {
			setLocalStorage(fixed_header_id, true);
			$(fixed_header_action).addClass(fixed_header_name);
		} else {
			setLocalStorage(fixed_header_id, false);
			$(fixed_header_action).removeClass(fixed_header_name);
		}
	});
	if (getLocalStorage(fixed_header_id) == 'true') {
		$('#'+fixed_header_id).click();
	} else {
		$(fixed_header_action).removeClass(fixed_header_name);
	}

	// Label
	$container.append('<h6 class="mt-3">Sidebar</h6>');

	// sidebar-collape
	sidebar_collape_id = 'sidebar-collape';
	sidebar_collape_text = 'Sidebar Collape';
	sidebar_collape_name = 'sidebar-collapse';
	sidebar_collape_action = 'body';
	createCheckBlock(sidebar_collape_text, sidebar_collape_id);
	$('body').on('click', '#'+sidebar_collape_id, function() {
		
		if ($(this).is(':checked')) {
			setLocalStorage(sidebar_collape_id, true);
			$(sidebar_collape_action).addClass(sidebar_collape_name);
		} else {
			setLocalStorage(sidebar_collape_id, false);
			$(sidebar_collape_action).removeClass(sidebar_collape_name);
		}
	});
	if (getLocalStorage(sidebar_collape_id) == 'true') {
		$('#'+sidebar_collape_id).click();
	} else {
		$(sidebar_collape_action).removeClass(sidebar_collape_name);
	}

	// fixed-header
	sidebar_nav_compact_id = 'sidebar-nav-compact';
	sidebar_nav_compact_text = 'Sidebar Navbar Compact';
	sidebar_nav_compact_name = 'nav-compact';
	sidebar_nav_compact_action = '.nav-sidebar';
	createCheckBlock(sidebar_nav_compact_text, sidebar_nav_compact_id);
	$('body').on('click', '#'+sidebar_nav_compact_id, function() {
		
		if ($(this).is(':checked')) {
			setLocalStorage(sidebar_nav_compact_id, true);
			$(sidebar_nav_compact_action).addClass(sidebar_nav_compact_name);
		} else {
			setLocalStorage(sidebar_nav_compact_id, false);
			$(sidebar_nav_compact_action).removeClass(sidebar_nav_compact_name);
		}
	});
	if (getLocalStorage(sidebar_nav_compact_id) == 'true') {
		$('#'+sidebar_nav_compact_id).click();
	} else {
		$(sidebar_nav_compact_action).removeClass(sidebar_nav_compact_name);
	}

	// Label
	$container.append('<h6 class="mt-3 mb-3">Sidebar Text</h6>');

	// accent_colors
	var $sidebar_text = $('<div />', {
		'class': 'd-flex'
	});
	$sidebar_text.append(createSkinBlock(accent_colors, 'accent_colors', '', function() {
		$('body').on('click', '*[data-type=accent_colors]', function(e) {
			e.preventDefault();
			color = $(this).data('class');
			body = $('body');
			accent_colors.map(function (skin) {
				body.removeClass(skin)
			});
			body.addClass(color);
			setLocalStorage('accent_colors', color);
		})
	}));
	if (getLocalStorage('accent_colors') != null) {
		accent_colors.map(function (skin) {
			$('body').removeClass(skin)
		});
		$('body').addClass(getLocalStorage('accent_colors'));
	}
	$container.append($sidebar_text);

	// Label
	$container.append('<h6 class="mb-3">Background Header</h6>');

	// background_header
	var $background_header = $('<div />', {
		'class': 'd-flex'
	});
	$background_header.append(createSkinBlock(navbar_skins, 'navbar_skins', 'navbar-dark navbar-light', function() {
		$('body').on('click', '*[data-type=navbar_skins]', function(e) {
			e.preventDefault();
			color = $(this).data('class');
			body = $('.main-header');
			navbar_skins.map(function (skin) {
				body.removeClass(skin)
			})
			body.addClass(color);
			setLocalStorage('navbar_skins', color);
		})
	}));
	if (getLocalStorage('navbar_skins') != null) {
		navbar_skins.map(function (skin) {
			$('.main-header').removeClass(skin)
		});
		$('.main-header').addClass(getLocalStorage('navbar_skins'));
	}
	$container.append($background_header);

	// Label
	$container.append('<h6 class="mb-3">Background Dark Sidebar</h6>');

	// background_sidebar
	var $background_dark_sidebar = $('<div />', {
		'class': 'd-flex'
	});
	$background_dark_sidebar.append(createSkinBlock(sidebar_dark_skins, 'sidebar_dark_skins', 'sidebar-dark-primary', function() {
		$('body').on('click', '*[data-type=sidebar_dark_skins]', function(e) {
			e.preventDefault();
			color = $(this).data('class');
			body = $('.main-sidebar');
			sidebar_dark_skins.map(function (skin) {
				body.removeClass(skin)
			});
			sidebar_light_skins.map(function (skin) {
				body.removeClass(skin)
			});
			body.addClass(color);
			setLocalStorage('sidebar_dark_skins', color);
		})
	}));
	if (getLocalStorage('sidebar_dark_skins') != null) {
		sidebar_dark_skins.map(function (skin) {
			$('.main-sidebar').removeClass(skin)
		});
		sidebar_light_skins.map(function (skin) {
			$('.main-sidebar').removeClass(skin)
		});
		$('.main-sidebar').addClass(getLocalStorage('sidebar_dark_skins'));
	}
	$container.append($background_dark_sidebar);

	// Label
	$container.append('<h6 class="mb-3">Background Light Sidebar</h6>');

	// background_sidebar
	var $background_light_sidebar = $('<div />', {
		'class': 'd-flex'
	});
	$background_light_sidebar.append(createSkinBlock(sidebar_light_skins, 'sidebar_light_skins', 'sidebar-dark-primary', function() {
		$('body').on('click', '*[data-type=sidebar_light_skins]', function(e) {
			e.preventDefault();
			color = $(this).data('class');
			body = $('.main-sidebar');
			sidebar_dark_skins.map(function (skin) {
				body.removeClass(skin)
			});
			sidebar_light_skins.map(function (skin) {
				body.removeClass(skin)
			});
			body.addClass(color);
			setLocalStorage('sidebar_light_skins', color);
		})
	}));
	if (getLocalStorage('sidebar_light_skins') != null) {
		sidebar_dark_skins.map(function (skin) {
			$('.main-sidebar').removeClass(skin)
		});
		sidebar_light_skins.map(function (skin) {
			$('.main-sidebar').removeClass(skin)
		});
		$('.main-sidebar').addClass(getLocalStorage('sidebar_light_skins'));
	}
	$container.append($background_light_sidebar);




	// Thêm khối checkbox
	function createCheckBlock(text, id) {
		$container.append(`
			<div class="mb-1">
				<input type="checkbox" class="btn-checkbox mr-1" id="${id}">
				<label for="${id}" class="text-sm m-0" style="font-weight: normal; cursor: pointer;">${text}</label>
			</div>
		`);
	}
	// Thêm khối màu
	function createSkinBlock(colors, type, clear_class, callback) {
		var $block = $('<div />', {
			'class': 'd-flex flex-wrap mb-3'
		});
		colors.map(function (color) {
			var $color = $('<div />', {
				'class': (typeof color === 'object' ? color.join(' ') : color).replace('navbar-', 'bg-').replace('accent-', 'bg-').replace('sidebar-dark-', 'bg-').replace('sidebar-light-', 'bg-') + ' elevation-2',
				'data-class': color,
				'data-type': type,
			});
			$block.append($color);
			$color.css({
				width       : '40px',
				height      : '20px',
				borderRadius: '25px',
				marginRight : 10,
				marginBottom: 10,
				opacity     : 0.8,
				cursor      : 'pointer'
			});
			$color.hover(function () {
				$(this).css({ opacity: 1 }).removeClass('elevation-2').addClass('elevation-4')
			}, function () {
				$(this).css({ opacity: 0.8 }).removeClass('elevation-4').addClass('elevation-2')
			})

		});
		$block.append('<a href="javascript:void(0)" data-type="'+type+'" data-class="'+clear_class+'">clear</a>');
		if (callback) {callback();}
		return $block;
	}
});