<?php
App::booted(function() {
	$namespace = 'Sudo\Theme\Http\Controllers\Admin';
	Route::namespace($namespace)->name('admin.')->prefix(config('app.admin_dir'))->middleware(['web', 'auth-admin'])->group(function() {
		// route chung cho cấu hình
		Route::name('settings.')->prefix('settings')->group(function() {
			// Cấu hình chung
			Route::match(['GET', 'POST'], 'general', 'SettingController@general')->name('general');
			// Cấu hình trang chủ
			Route::match(['GET', 'POST'], 'home', 'SettingController@home')->name('home');
			// Cấu hình SEO
			Route::match(['GET', 'POST'], 'seo', 'SettingController@seo')->name('seo');
		});
	});
});