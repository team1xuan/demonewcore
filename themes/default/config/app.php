<?php 

return [
	// Đa ngôn ngữ
    'language' => [
        'vi' => [
            'name' => 'Tiếng việt',
            'flag' => '/core/img/flags/vn.png',
            'locale' => 'vi_VN',
        ],
        'en' => [
            'name' => 'English',
            'flag' => '/core/img/flags/en.png',
            'locale' => 'en_EN',
        ],
    ],

    // Menu form
    'menu_form' => [
        // Cố định
        'menu_link' => [
            'vi' => [
                '/' => 'Trang chủ',
                '/lien-he' => 'Liên hệ',
            ],
            'en' => [
                '/' => 'Home Page',
                '/contact' => 'Contact',
            ],
            'uk' => [
                '/' => 'Home Page',
                '/contact' => 'Contact',
            ]
        ],
        // Theo bảng
        'table_link' => [
            'pages' => [
                'name' => 'Trang đơn',
                'models' => 'Sudo\Theme\Models\Page',
                'has_locale' => true
            ],
            'post_categories' => [
                'name' => 'Danhh mục bài viết',
                'models' => 'Sudo\Theme\Models\PostCategory',
                'has_locale' => true
            ],
        ],
    ], 

    // Models của giao diện bên ngoài chứa hàm getUrl
    'page_models' => '\Sudo\Theme\Models\Page',
    'post_models' => '\Sudo\Theme\Models\Post',
    'post_category_models' => '\Sudo\Theme\Models\PostCategory',
];