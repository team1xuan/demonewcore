<?php

namespace Sudo\Theme\Http\Controllers\Web;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{

	function __construct() {
		// 

		\Asset::addStyle(['style'])
				->addScript(['jquery', 'functions', 'main']);

		$this->middleware(function ($request, $next) {
			setLanguage(\Session::get('locale'));
			// Cache cấu hình general
	        $config_general = getOption('general');
	        \View::share('config_general',$config_general);

	        // Cache cấu hình SEO
	        $config_seo = getOption('seo');
	        \View::share('config_seo',$config_seo);

	        return $next($request);
		});
	}

}