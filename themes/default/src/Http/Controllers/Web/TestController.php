<?php

namespace Sudo\Theme\Http\Controllers\Web;

use Illuminate\Http\Request;

use Analytics;
use Spatie\Analytics\Period;
use Illuminate\Support\Carbon;

class TestController extends Controller
{
	public function index(Request $request) {

		// Nước truy cập
		// $analyticsData = Analytics::performQuery(
		// 	Period::days(0),
		// 	'ga:sessions',
		// 	[
		// 		'dimensions' => 'ga:country',
		// 		'sort' => 'ga:country',
		// 	]
		// );
		// dump($analyticsData);

		$analyticsData = Analytics::performQuery(
            Period::days(0),
            'ga:users,ga:pageviews',
            [
            	'start-date' => '2020-05-20',
                'end-date' =>  '2020-05-20',
            	'dimensions' => 'ga:date'
            ]
        );
		dump($analyticsData->rows);

		die;
		// // maps
		// $stt = 0;
		// for ($j=0; $j < 100; $j++) { 
	 //        $post_category_maps = [];
		// 	for ($i=0; $i < 1000; $i++) {
		// 		$stt++;
	 //            $post_category_maps[] = [
	 //                'post_id' => $stt,
	 //                'post_category_id' => rand(1,10),
	 //            ];
		// 	}
  //       	\DB::table('post_category_maps')->insert($post_category_maps);
		// }
		
		dd('Chức năng khóa!');
	}
}