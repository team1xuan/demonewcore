<?php

namespace Sudo\Theme\Http\Controllers\Admin;
use Sudo\Base\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use DB;
use Form;

class SettingController extends AdminController
{
	function __construct() {
		parent::__construct();
        $this->models = new \Sudo\Theme\Models\Setting;
        $this->table_name = $this->models->getTable();
	}

	// Cấu hình chung
	public function general(Request $requests) {
		$setting_name = 'general';
        $title = "Cấu hình chung";
        $note = "Form::form.require_text";
        // Thêm hoặc cập nhật dữ liệu
        if (isset($requests->redirect)) {
        	$this->models->postData($requests, $setting_name);
        }
        // Lấy dữ liệu ra
        $data = $this->models->getData($setting_name);
		// Khởi tạo form
        $form = new Form;

        $form->customMenu('primary_menu', $data['primary_menu'] ?? '', 'Menu Chính');
        $form->customMenu('footer_menu', $data['footer_menu'] ?? '', 'Menu Footer');
        
        $form->action('editconfig');
        // Hiển thị form tại view
        return $form->render('custom', compact(
        	'title', 'note'
        ), 'Default::admin.settings.form');
    }

    public function home(Request $requests) {
        $setting_name = 'home';
        $title = "Cấu hình trang chủ";
        $note = "Form::form.require_text";
        // Thêm hoặc cập nhật dữ liệu
        if (isset($requests->redirect)) {
            $this->models->postData($requests, $setting_name);
        }
        // Lấy dữ liệu ra
        $data = $this->models->getData($setting_name);
        // Khởi tạo form
        $form = new Form;

        $form->text('meta_title', $data['meta_title'] ?? '', 0, 'Meta Title');
        $form->textarea('meta_description', $data['meta_description'] ?? '', 0, 'Meta Description');
        $form->image('meta_image', $data['meta_image'] ?? '', 0, 'Meta Image');

        $form->action('editconfig', route('app.home'));
        // Hiển thị form tại view
        return $form->render('custom', compact(
            'title', 'note'
        ), 'Default::admin.settings.form');
    }

    public function seo(Request $requests) {
        $setting_name = 'seo';
        $title = "Cấu hình SEO";
        $note = "Việc Cài đặt SAI có thể ảnh hưởng nghiêm trọng tới hiệu suất SEO Website của bạn. Hãy cân nhắc kỹ.";
        // Thêm hoặc cập nhật dữ liệu
        if (isset($requests->redirect)) {
            $this->models->postData($requests, $setting_name);
        }
        // Lấy dữ liệu ra
        $data = $this->models->getData($setting_name);
        // Khởi tạo form
        $form = new Form;

        $form->radio('robots', $data['robots'] ?? '', 'Cho phép google lập chỉ mục nội dung Website', [ 1 => 'Có', 0 => 'Không', ]);
        $form->textarea('html_head', $data['html_head'] ?? '', 0, 'Các thẻ html chèn vào head');
        $form->textarea('html_body', $data['html_body'] ?? '', 0, 'Các thẻ html chèn vào sau thẻ mở body');
        $form->textarea('html_body', $data['html_body_end'] ?? '', 0, 'Các thẻ html chèn vào trước thẻ đóng body');
        $form->textarea('html_body', $data['style_custom'] ?? '', 0, 'Cấu hình CSS');

        $form->title('Cấu hình mạng xã hội');
        $form->text('facebook', $data['facebook'] ?? '', 0, 'Facebook');
        $form->text('youtube', $data['youtube'] ?? '', 0, 'Youtube');
        $form->text('linkedin', $data['linkedin'] ?? '', 0, 'Linkedin');
        $form->text('twitter ', $data['twitter '] ?? '', 0, 'Twitter');

        $form->title('Liên hệ');
        $form->text('meta_title[contact]', $data['meta_title']['contact'] ?? '', 0, 'Meta Title');
        $form->textarea('meta_description[contact]', $data['meta_description']['contact'] ?? '', 0, 'Meta Description');

        $form->action('editconfig');
        // Hiển thị form tại view
        return $form->render('custom', compact(
            'title', 'note'
        ), 'Default::admin.settings.form');
    }
}