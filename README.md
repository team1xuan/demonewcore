# Hướng dẫn bắt đầu dự án thiết kế web với sudo core website từ repo startproject

**Lưu ý:** 

* Để bắt thực hiện được bạn cần phải có quyền access vào sudo-base repository (nới lưu trữ core)
* Các thao tác với git thực hiện qua giao thức SSH, đảm bảo máy local đã setup SSH key (xem hướng dẫn: [https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html))
* Khuyến khích sử dụng môi trường docker để dev dự án



## Với môi trường docker
### 1. Clone repository

`git clone git@bitbucket.org:taitt/sudo-startproject.git your-project-name`

Chú ý: `your-project-name` là tên dự án - tên thư mục của bạn.


### 2. Tạo file .env từ file .env.example

Vào dự án: `cd your-project-name`

Tạo file: `cp .env.example .env` 

### 3. Sửa cấu hình file .env
* `NAME_CONTAINER=your_app_name` là tên được đặt để chạy trên docker, ví dụ: `chanhtuoi`. Đảm bảo rằng nó chưa được sử dụng bởi dự án khác.
* `PORTS_NGINX=9001` 9001 là ví dụ, hãy đảm bảo nó chưa được sử dụng bởi dự án khác.
* `PORTS_MYSQL=3001` hãy đảm bảo 3001 cũng chưa được sử dụng bởi dự án hay ứng dụng khác.
* `DB_HOST=your_app_name_mysql` phải có hậu tố _mysql vì docker-compose đã quy định, ví dụ: `chanhtuoi_mysql`
* `DB_DATABASE=your_app_name`
* `DB_USERNAME=root`
* `DB_PASSWORD=secret`

### 4. Chạy môi trường
* Khởi động docker desktop app
* Chạy: `docker-compose up -d`
* Truy cập vào máy: `docker exec -it your_app_name_php-fpm bash` ví dụ: `docker exec -it chanhtuoi_php-fpm bash`
* Chuyển thư mục làm việc: `cd var/home`
* List thử file: `ls -la`

### 5. Thiết lập và khởi chạy
* Chạy `php artisan key:generate` để tạo key cho ứng dụng laravel
* Chạy `composer update` để cập nhật các package
* Chạy `php artisan migrate` để sinh các bảng dữ liệu
* Chạy `php artisan sudo:seeds` để sinh dữ liệu
* Chạy `php artisan vendor:publish --tag=sudo/core` để publish các file config và giao diện
* Truy cập đường dẫn `http://localhost:9001/{admin_dir}/login` để đăng nhập, trong đó `admin_dir` là đường dẫn admin được đặt tại `config('app.admin_dir')`

Lưu ý khi chạy composer update trên máy docker sẽ hỏi mật khẩu của rsa key, hãy nhập: `sudovn`


## Với môi trường xampp
### 1. Clone repository

`git clone git@bitbucket.org:taitt/sudo-startproject.git your-project-name`

Chú ý: `your-project-name` là tên dự án - tên thư mục của bạn.


### 2. Tạo file .env từ file .env.example

Duplicate file `.env.example` rồi đổi tên thành `.env`

Hoặc tạo file: `.env` rồi copy nội dung từ `.env.example`

### 3. Sửa cấu hình file .env
* `DB_DATABASE=your_db_name`
* `DB_USERNAME=root`
* `DB_PASSWORD=`

### 4. Chạy môi trường
* Thiết lập cổng cho dự án, tạo db
* Chạy xampp

### 5. Thiết lập và khởi chạy
* Chạy `php artisan key:generate` để tạo key cho ứng dụng laravel
* Chạy `composer update` để cập nhật các package
* Chạy `php artisan migrate` để sinh các bảng dữ liệu
* Chạy `php artisan sudo:seeds` để sinh dữ liệu
* Chạy `php artisan vendor:publish --tag=sudo/core` để publish các file config và giao diện
* Truy cập đường dẫn `http://localhost:9001/{admin_dir}/login` để đăng nhập, trong đó `admin_dir` là đường dẫn admin được đặt tại `config('app.admin_dir')`



